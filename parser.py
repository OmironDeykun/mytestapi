import json
import sys
import requests

from lxml import html


def get_parsed_page(url):
    response = requests.get(url)
    parsed_page = html.fromstring(response.content)

    return parsed_page


def get_history(url_to_get):
    result = {}
    for a in range(12):
        a += 1
        if a < 10:
            a = '0' + str(a)
        parsed_page = get_parsed_page(url_to_get+str(a)+'/2018')

        page_temps_xpath = '//a[@class!="item inactive"]/div[@class="i4"]/text()'
        page_dates_xpath = '//a[@class!="item inactive"]/div[@class="i2"]/span/text()'

        temps = parsed_page.xpath(page_temps_xpath)
        dates = parsed_page.xpath(page_dates_xpath)

        i = 0
        res = {}
        for date in dates:

            date = ''.join(filter(str.isdigit, date))
            date = date+'.'+str(a)+'.2018'
            res[date] = temps[i]

            i += 1

        result.update(res)

    return result


if __name__ == "__main__":
    if len(sys.argv) == 3:
        url_to_get = sys.argv[1]
        url_to_send = sys.argv[2]

    if 'url_to_send' not in locals():
        url_to_send = 'http://127.0.0.1:5000/temperature'

    if 'url_to_get' not in locals():
        url_to_get = 'https://pogoda.vtomske.ru/tomsk/archive/'

    payload = {'some': get_history(url_to_get)}
    headers = {'content-type': 'application/json'}

    response = requests.post(url_to_send, data=json.dumps(payload), headers=headers)
